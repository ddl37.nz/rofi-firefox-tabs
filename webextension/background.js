// Time intervals in milliseconds
const CONF = {
    // Enable increased console.log-ing
    DEBUG_MODE: true,
    // How often to unconditionally refresh and push tab list
    POLL_INTERVAL: 1000 * 60 * 10,
    // Restrict number of tab pushes to 1 per DEBOUNCE_INTERVAL
    DEBOUNCE_INTERVAL: 1000,
    // On browser start or when websocket connection lost, how often to attempt reconnection
    SOCKET_ATTEMPT_INTERVAL: 5000,
    INCLUDE_PRIVATE_TABS: false,
}
const wait = ms => new Promise((yay, nay) => setTimeout(yay, ms))
const log = (...args) => CONF.DEBUG_MODE ? (args.length === 1 ? console.log(args[0]) : console.log(args)) : null

browser.storage.local.get("include-private-tabs").then(values => {
    if (values['include-private-tabs'] === true)
        CONF.INCLUDE_PRIVATE_TABS = true
})

///

let handle = (function create_websocket_handler(address) {
    let socket = null

    function try_connect(addr) {
        let s = new WebSocket(addr)

        return new Promise((yay, nay) => {
            s.onopen = () => yay(s)
            s.onclose = nay
            s.onerror = nay
        })
    }

    function is_ready(socket) {
        // WebSocket :: readyState
        // 0 - CONNECTING
        // 1 - OPEN
        // 2 - CLOSING
        // 3 - CLOSED

        return socket !== null && socket.readyState === 1
    }

    async function get_handle() {
        if (is_ready(socket)) return socket

        socket = null // Socket is closed or closing
        while (true) {
            try {
                socket = await try_connect(address)
                if (is_ready(socket))
                    break
            } catch (err) {
                console.warn(err)
            }
            console.warn(`[WARN] Failed to open websocket @ ${address} (ensure rofi-server is running)`)
            await wait(CONF.SOCKET_ATTEMPT_INTERVAL)
        }
        log(`[INFO] Opened websocket @ ${address}`)
        return socket
    }

    return {
        get: get_handle
    }
})("ws://127.0.0.1:8081")

function activate_tab(tab) {
    browser.tabs.update(tab.id, { active: true })
    browser.windows.update(tab.windowId, { focused: true, drawAttention: true })
}

async function list_browser_tabs() {
    let tabs = (await browser.tabs.query({}))
        .filter(t => CONF.INCLUDE_PRIVATE_TABS ? true : !t.incognito)

    // Don't send irrelevant fields like faviconID, etc...
    return tabs.map(({ id, active, url, title }) => { return { id, active, url, title } })
}

// Refreshes the websocket handle and pushes updated tab list to rofi-server
async function event_tick() {
    let socket = await handle.get()

    socket.onmessage = msg => {
        let tab_id = Number.parseInt(msg.data)
        log(`[INFO] Switch requested to tab id: ${tab_id}`)

        browser.tabs.get(tab_id)
            .then(activate_tab)
            .catch(err => { console.error("Requested tab not found (probably closed)") })
    }

    socket.send(JSON.stringify(await list_browser_tabs()))
}

/*
Returns a `debounced_callback` function that "restricts" the rate at which events make it through to the actual `callback` function. Events between "bounces" will be skipped. IDEA: Can return array of events
*/
function debounced(callback, bounce_ms) {
    let last_called_ms = 0
    let last_timeout = 0
    let debounced_callback = (...args) => { // Return values of callback are irrelevant
        clearTimeout(last_timeout)
        let delta = Date.now() - last_called_ms
        if (delta >= bounce_ms) {
            last_called_ms = Date.now()
            callback(...args)
        } else {
            last_timeout = setTimeout(() => debounced_callback(...args), bounce_ms - delta + 1)
        }
    }
    return debounced_callback
}

// Returns 2 callback functions that get mapped into `Left` and `Right` events in an infinite async generator stream (debounced). Code would probably be nicer in Rust. 
function async_select(debounce_interval_ms) {
    // Events occurring before loop() is awaited, are ignored
    let yay = () => { console.warn("async_select has unresolved event") }

    let cb = (left, right) => yay([left, right]) // Promise resolution accepts only one argument
    let de_cb = debounced(cb, debounce_interval_ms)
    let left_cb = arg => de_cb(arg, null)
    let right_cb = arg => de_cb(null, arg)

    function next() {
        return new Promise((y, n) => { yay = y })
    }

    async function* loop() {
        while (true) {
            yield await next()
        }
    }

    return {
        left_cb,
        right_cb,
        loop,
    }
}

(async function main() {
    let { left_cb, right_cb, loop } = async_select(CONF.DEBOUNCE_INTERVAL)

    // left_cb is called with id of the tab that was added/updated/removed
    // unused for now but could eventually implement diffing to be slightly more efficient
    browser.tabs.onCreated.addListener((tab) => {
        if (tab.url !== "about:newtab")
            left_cb(tab.id)
    })

    browser.tabs.onUpdated.addListener((tab_id, change, tab) => {
        if (change.status === "complete")
            left_cb(tab_id)
    }, { properties: ["status"] })

    // Without delay sometimes tab list still includes deleted tab. Problematic since might be very long time until next tab event/poll
    browser.tabs.onRemoved.addListener(id => setTimeout(() => left_cb(id), 500))

    // Push tabs every now and then to keep connection alive
    setInterval(right_cb, CONF.POLL_INTERVAL)

    await event_tick()
    for await (let [left, right] of loop()) {
        log([left, right])
        await event_tick()
    }
})().catch(err => { throw err })
