
// Display current preference value
browser.storage.local.get("include-private-tabs").then(values => {
    if (values["include-private-tabs"] === true) {
        allow.checked = true
    } else {
        deny.checked = true
    }
})

function on_pref_update() {
    console.log("Updated private tab preference")
    info.innerHTML = "Preference will be applied on browser restart"
}

// False by default (don't send)
allow.onclick = () => {
    browser.storage.local.set({
        "include-private-tabs": true
    }).then(on_pref_update)
}

deny.onclick = () => {
    browser.storage.local.set({
        "include-private-tabs": false
    }).then(on_pref_update)
}
