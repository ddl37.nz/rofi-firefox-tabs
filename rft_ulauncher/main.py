from ulauncher.api.client.Extension import Extension
from ulauncher.api.client.EventListener import EventListener
from ulauncher.api.shared.event import KeywordQueryEvent, ItemEnterEvent
from ulauncher.api.shared.item.ExtensionResultItem import ExtensionResultItem
from ulauncher.api.shared.action.RenderResultListAction import RenderResultListAction
from ulauncher.api.shared.action.HideWindowAction import HideWindowAction
from ulauncher.api.shared.action.ExtensionCustomAction import ExtensionCustomAction

import socket
import json

socket.setdefaulttimeout(0.5)


def read_socket(s: socket.socket, BUFFER_SIZE=4096):
    buffer = b""
    while True:
        buffer_part = s.recv(BUFFER_SIZE)
        buffer += buffer_part

        if len(buffer_part) < BUFFER_SIZE:
            return buffer.decode('utf-8')


def get_tabs():
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(('127.0.0.1', 8082))

    client.send('GET'.encode('utf8'))
    response = read_socket(client)
    return json.loads(response)


def set_tab(target):
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(('127.0.0.1', 8082))

    client.send(f'SET {target}'.encode('utf8'))


def tab_to_result(t):
    return ExtensionResultItem(
        icon="images/icon.png",
        name=t["title"],
        description=t["url"],
        on_enter=ExtensionCustomAction(t["title"], keep_app_open=False)
    )


class RFTExtension(Extension):
    def __init__(self):
        super().__init__()
        self.subscribe(KeywordQueryEvent, KeywordQuery())
        self.subscribe(ItemEnterEvent, ItemEnter())


class KeywordQuery(EventListener):
    def on_event(self, event, extension):
        q = event.get_query().lower()
        if q.startswith("tab:"):
            q = q[4:].strip()
        # it's quite possible to reach a number of tabs for which this actually becomes a bottleneck...
        matching = map(tab_to_result, filter(lambda t: q in t["title"].lower() or q in t["url"].lower(), get_tabs()))
        return RenderResultListAction(list(matching))


class ItemEnter(EventListener):
    def on_event(self, event, extension):
        set_tab(event.get_data())


if __name__ == '__main__':
    RFTExtension().run()
