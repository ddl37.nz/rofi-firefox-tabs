# rofi-firefox-tabs

Fetches a list of all currently open tabs in Firefox and displays an interface to switch between them. Despite the project name, several interfaces are supported:
- **rofi**
- **ulauncher**
- **fzf**

This project is occasionally maintained. Feature suggestions, issues and merge requests are welcome.

## Usage

### Option 1: rofi in dmenu mode

The script at `script/rofi_tabs.sh` will start Rofi. You can bind this to a hotkey for convenience.

![Rofi run in -dmenu mode using rofi_tabs.sh](screenshots/screenshot.png)

### Option 2: rofi mod[ei]

either via config file:
```
configuration {
	modi: "window,drun,tabs:python <PATH-TO-PROJECT-ROOT>/rofi-firefox-tabs/script/tabs.py";
}
```
or directly from the command line:
```
rofi -show tabs -modi tabs:"python <PATH-TO-PROJECT-ROOT>/rofi-firefox-tabs/script/tabs.py"
```

> URLs are not shown in this mode due to lack of formatting

### Option 3: via ulauncher extension

Copy or symlink the `rft_ulauncher` folder into `~/.local/share/ulauncher/extensions` and restart ulauncher.

![I don't have as many tabs in this screenshot as it's a new computer lol](screenshots/screenshot-2022a.png)

You can have `tab:` as the initial text in the input by hacking on the ulauncher source code. You can also make the window wider to prevent truncation of long tab titles.

### Option 4: using fzf

For the Fish shell:
```fish
alias jt="python path/to/script/tabs-fzf.py (python path/to/script/tabs-fzf.py | fzf --ansi)"
funcsave jt
```

- [ ] Feature idea: Index page content and display a Markdown preview

The `tabdump.py` script dumps the URLs of all tabs to standard output, which you could then pipe into a text editor for further processing. 

## Installation

1. Clone this repo
2. `about:addons` in Firefox > ⚙️ > Install add-on from file > Latest zip file in `/web-ext-artifacts`
> You may need to disable extension signing: `xpinstall.signatures.required: false` in `about:config`
3. Ensure Rust is installed. Then run `cargo install --path .` in the `server` folder. The compiled binary will (probably) be at `~/.cargo/bin/rofi-server`, and needs to be running for tabs to be shown. It's convenient to have it start automatically, e.g. through a systemd service:
```
cp rofi-server.service ~/.config/systemd/user
systemctl --user enable --now rofi-server.service
```
4. Done! Choose one of the options above and enjoy a better search experience for the hundreds of tabs you have open.

### Potential problems
* If the ports 8081 and 8082 are in use by another application this program probably won't work. Those values are currently hard-coded in `tabs.py`, `background.js` and `main.rs`
* Depending on the DE/WM, switching to a tab may or may not focus the correct browser window. In GNOME, the 'NoAnnoyance v2' shell extension fixes this (for me at least, ymmv)
