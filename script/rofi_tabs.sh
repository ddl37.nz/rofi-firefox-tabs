#!/bin/bash
SCRIPT_PATH=$(dirname $(realpath -s $0))
TABNAME=$(python $SCRIPT_PATH/tabs_get.py | rofi -dmenu -i -markup-rows -scroll-method 1 -matching normal -tokenize)
echo $TABNAME | python $SCRIPT_PATH/tabs_set.py
