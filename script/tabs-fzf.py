#!/usr/bin/env python
import sys
from tabs import TabClient


class BLU:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


SEP="  "
c = TabClient(lambda t: f"{t['title']}{SEP}{BLU.OKCYAN}{t['url']}{BLU.ENDC}")

if len(sys.argv) > 1:
    fzf_out = sys.argv[1]
    c.set_tab_with_title(fzf_out.rsplit(SEP, maxsplit=1)[0])
else:
    c.print()
